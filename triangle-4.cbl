       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-4.
       AUTHOR. THUN.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE          PIC X(80) VALUE SPACES .
       01  STAR-NUM          PIC 9(3)  VALUE ZERO .
           88 VALID-STAR-NUM VALUE 0 THRU 80 .
       01  INDEX-NUM1        PIC 9(3)  VALUE ZERO .
       01  INDEX-NUM2        PIC 9(3)  VALUE ZERO .
       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-INPUT-STAR-NUMBER THRU 001-EXIT 
           PERFORM VARYING INDEX-NUM1 FROM STAR-NUM BY -1
              UNTIL INDEX-NUM1 = 0 
              COMPUTE INDEX-NUM2 = STAR-NUM - INDEX-NUM1 + 1
              MOVE ALL SPACE TO SCR-LINE 
              MOVE ALL "*" TO SCR-LINE (INDEX-NUM2 :INDEX-NUM1)
              DISPLAY SCR-LINE 
           END-PERFORM
           GOBACK
           .
       001-INPUT-STAR-NUMBER.
           PERFORM UNTIL STAR-NUM > 0 AND STAR-NUM <= 80
              DISPLAY "Please inpuut star number: "WITH NO ADVANCING 
              ACCEPT STAR-NUM 
              IF STAR-NUM = 0 
              DISPLAY "Please input star number is positive number"
           END-PERFORM          
           .
       001-EXIT.
           EXIT
           .