       IDENTIFICATION DIVISION. 
       PROGRAM-ID. STAR-10-1.
       AUTHOR. THUN.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-PRINT-STAR-IN-LINE THRU 001-EXIT
           PERFORM 002-PRINT-STAR-OUT-LINE THRU 002-EXIT
           GOBACK
       .
       001-PRINT-STAR-IN-LINE.
           PERFORM 10 TIMES 
              DISPLAY "*" WITH NO ADVANCING 
           END-PERFORM 
           DISPLAY ""
           .
       001-EXIT.
           EXIT
           .
       002-PRINT-STAR-OUT-LINE.
           PERFORM 003-PRINT-ONE-STAR 10 TIMES
           .
       002-EXIT.
           EXIT 
           .
       003-PRINT-ONE-STAR.
           DISPLAY "*"
           .
