       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LIST6-4.
       AUTHOR. THUN.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  COUNTERS.
           05 HUNDRED-COUNT  PIC 99 VALUE ZERO.
           05 TEN-COUNT      PIC 99 VALUE ZERO.
           05 UNIT-COUNT     PIC 99 VALUE ZERO.
       01  ODOMETER.
           05 PRN-HUNDRED    PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-TEN        PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-UNIT       PIC 9.
       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform"
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
              VARYING HUNDRED-COUNT FROM 0 BY 1 UNTIL HUNDRED-COUNT > 9
              AFTER TEN-COUNT FROM 0 BY 1 UNTIL TEN-COUNT > 9
              AFTER UNIT-COUNT FROM 0 BY 1 UNTIL UNIT-COUNT > 9
      *     PERFORM VARYING HUNDRED-COUNT FROM 0 BY 1 
      *              UNTIL HUNDRED-COUNT > 9
      *     PERFORM VARYING TEN-COUNT FROM 0 BY 1 UNTIL TEN-COUNT > 9
      *     PERFORM VARYING UNIT-COUNT FROM 0 BY 1 UNTIL UNIT-COUNT > 9
      *        MOVE HUNDRED-COUNT TO PRN-HUNDRED 
      *        MOVE TEN-COUNT TO PRN-TEN
      *        MOVE UNIT-COUNT TO PRN-UNIT
      *        DISPLAY "Out - " ODOMETER
      *     END-PERFORM 
      *     END-PERFORM
      *     END-PERFORM
           GOBACK 
           .
       001-COUNT-MILEAGE.
           MOVE HUNDRED-COUNT TO PRN-HUNDRED 
           MOVE TEN-COUNT TO PRN-TEN
           MOVE UNIT-COUNT TO PRN-UNIT
           DISPLAY "Out - " ODOMETER 
           .
       001-EXIT.
           EXIT
           .
